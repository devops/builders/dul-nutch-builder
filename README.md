# DUL Java Builder

## Create ImageStreams
```bash
$ oc login ...
$ oc create -f openshift/templates/imageStream-nutch-builder.yaml
$ oc create -f openshift/templates/imageStream-dul-nutch.yaml
```

## Create BuildConfigs
```bash
$ oc login ...
$ oc create -f openshift/templates/buildConfig-nutch-builder.yaml
$ oc create -f openshift/templates/buildConfig-dul-nutch.yaml
```

