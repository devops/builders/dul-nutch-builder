# apache-nutch-builder
FROM openjdk:8-jre-buster
LABEL maintainer "Duke University Libraries / ITS DevOps"

ENV STI_SCRIPTS_PATH=/usr/libexec/s2i \
    NUTCH_DIR=/opt/apache-nutch \
    NUTCH_USERNAME=nutch

ENV PATH=$NUTCH_DIR/bin:$PATH

LABEL io.k8s.description="Builder image for running Apache Nutch 1.18" \
      io.k8s.display-name="dul-nutch-builder" \
      io.openshift.s2i.scripts-url=image://${STI_SCRIPTS_PATH}

# Install cron
RUN apt-get update && apt-get install -y cron procps
COPY nutch-crontab /etc/cron.d/nutch-crontab
RUN chmod 0644 /etc/cron.d/nutch-crontab \
      && crontab /etc/cron.d/nutch-crontab

COPY ./distrib/ ${NUTCH_DIR}
COPY ./s2i/bin/ ${STI_SCRIPTS_PATH}

RUN chmod -R a+rx ${STI_SCRIPTS_PATH}

# make sure /etc/passwd is writable by gid=0 
# so the 'uid_entrypoint' script works correctly
#
# Inspired from:
# https://gitlab.oit.duke.edu/linux-at-duke/lad-mirror/-/blob/development/ovirt/Dockerfile
ADD nutch.inject uid_entrypoint /

RUN chmod u+x /uid_entrypoint \
      && touch /etc/passwd \
      && chmod 0644 /etc/passwd \
      && chmod -R g=u /etc/passwd \
      && ls -al /etc/passwd
# END inspiration

RUN mkdir -p $NUTCH_DIR \
      && chmod -R 0777 $NUTCH_DIR
      # && chown -R ${NUTCH_USERNAME} $NUTCH_DIR

WORKDIR $NUTCH_DIR

# "nutch startserver" will run under 8081
# "nutch webapp" runs under port 8080
EXPOSE 8081
EXPOSE 8080

USER 1001

ENTRYPOINT ["/uid_entrypoint"]
CMD ["/usr/libexec/s2i/usage"]
